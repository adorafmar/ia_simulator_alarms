var fs = require('fs')
var express = require('express');
var app = express(); 
const querystring = require("querystring");
const http = require('http');
var https = require('https') 
const port = 8089

app.get('/', function (req, res) {
	res.header('Access-Control-Allow-Origin' , '*');
	res.send('Server is on line')
})

app.get('/main/system/webdev/alarms/alarms', function (req, res) {
	fs.readFile('simulation_data.json', 'utf8', function (err,data) {
		if (err) {
			return console.log(err);
		}
		res.send(data);
	});
})

const httpServer = http.createServer(app);
httpServer.listen(port, () => {
    console.log('HTTP Server running on port ' + port);
});

// const httpsServer = https.createServer({
//   key: fs.readFileSync('id_rsa'),
//   cert: fs.readFileSync('outagemap_utilities_utexas_edu_cert.cer')
// }, app);
// httpsServer.listen(8443, () => {
//     console.log('HTTPS Server running on port 8443');
// });